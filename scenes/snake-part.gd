extends Node2D

class_name SnakePart

const SnakeDirection = preload("res://scripts/snake-direction.enum.gd").SnakeDirection
const SnakePartType = preload("res://scripts/snake-part-type.enum.gd").SnakePartType

const Up := SnakeDirection.Up
const Down := SnakeDirection.Down
const Right := SnakeDirection.Right
const Left := SnakeDirection.Left

# Sprite
var animated_sprite: AnimatedSprite

# Type de partie du serpent
var type: int

# Direction du morceau de serpent
# (c'est la direction vers l'avant du serpent, dans le cas d'un angle)
var direction: int = -1

# Direction des morceaux suivant et précédent
var next_direction: int = -1
var previous_direction: int = -1

# Numéro du morceau
var index: int

func _ready():
	pass

func update() -> void:
	pass
	
func set_anim(animation: String, frame := 0) -> void:
	animated_sprite.animation = animation
	animated_sprite.frame = frame

func update_display() -> void:
	animated_sprite = find_node('AnimatedSprite')
	
	# Type de morceau
	if next_direction == -1:
		type = SnakePartType.Head
	elif previous_direction == -1:
		type = SnakePartType.Tail
	else:
		type = SnakePartType.Body
	
	# Définir l'apparence en fonction du type
	match type:
		# Affichage d'une partie du corps
		SnakePartType.Body:
			animated_sprite.play()
			set_anim(get_body_animation(direction, next_direction), index % 2)
		
		# Affichage de la tête ou la queue
		SnakePartType.Head, SnakePartType.Tail:
			animated_sprite.stop()
			match type:
				SnakePartType.Head:
					set_anim('head', direction)
				SnakePartType.Tail:
					# La queue prend la direction du morceau auquel elle est raccrochée
					set_anim('tail', next_direction)

func get_body_animation(dir: int, next_dir: int) -> String:
	# Affichage d'une partie rectiligne du corps
	if next_dir == -1 or dir == next_dir:
		match dir:
			Up, Down:
				return 'vertical'
			Left, Right:
				return 'horizontal'
	
	# Affichage d'un angle
	else:
		if next_dir == Up:
			if dir == Right:
				return 'body-corner-4'
			elif dir == Left:
				return 'body-corner-1'
		elif next_dir == Down:
			if dir == Right:
				return 'body-corner-3'
			elif dir == Left:
				return 'body-corner-2'
		elif next_dir == Right:
			if dir == Up:
				return 'body-corner-2'
			elif dir == Down:
				return 'body-corner-1'
		elif next_dir == Left:
			if dir == Up:
				return 'body-corner-3'
			elif dir == Down:
				return 'body-corner-4'
	
	push_error("Erreur: impossible de trouver une animation pour les directions " + str(dir) + " et " + str(next_dir))
	return ''
