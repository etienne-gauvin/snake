extends Node2D

class_name Snake

const SnakeDirection := preload("res://scripts/snake-direction.enum.gd").SnakeDirection
const SnakePartType := preload("res://scripts/snake-part-type.enum.gd").SnakePartType

const SnakePart := preload("res://scenes/snake-part.tscn")

# Morceaux du serpent
const snake_parts := []

# Vitesse du serpent (1/t)
export var speed := 5

# Temps écoulé depuis le dernier déplacement
var time_since_last_move := 0.0

# Grandir de X lors des prochains déplacements
var grow := 4

# Direction
var current_direction: int
var next_direction: int

# Compteur d'itérations
var counter := 0

# Lors de l'appui sur une touche du clavier
func _input(event):
	# Le serpent se déplace actuellement de manière verticale ou horizontale
	var horizontal_move: bool = current_direction == SnakeDirection.Left or current_direction == SnakeDirection.Right
	var vertical_move: bool = current_direction == SnakeDirection.Up or current_direction == SnakeDirection.Down
	
	# Application du changement de direction
	if event.is_action_pressed("ui_left") and vertical_move:
		next_direction = SnakeDirection.Left
	elif event.is_action_pressed("ui_right") and vertical_move:
		next_direction = SnakeDirection.Right
	elif event.is_action_pressed("ui_up") and horizontal_move:
		next_direction = SnakeDirection.Up
	elif event.is_action_pressed("ui_down") and horizontal_move:
		next_direction = SnakeDirection.Down

# Called when the node enters the scene tree for the first time.
func _ready():
	print(current_direction)
	next_direction = SnakeDirection.Up
	current_direction = next_direction
	
	update_display_and_neighbors(add_part(current_direction, 100, 116))
	update_display_and_neighbors(add_part(current_direction, 100, 108))
	update_display_and_neighbors(add_part(current_direction, 100, 100))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_since_last_move += delta
	
	if time_since_last_move > 1.0 / speed:
		time_since_last_move = 0.0
		move()

# Déplacement du serpent d'un cran
func move() -> void:
	
	print('=================================')
	print('======   Moving forward   =======')

	var next_x: int = get_head().position.x
	var next_y: int = get_head().position.y
	
	match next_direction:
		SnakeDirection.Up:
			next_y -= 8
		SnakeDirection.Down:
			next_y += 8
		SnakeDirection.Right:
			next_x += 8
		SnakeDirection.Left:
			next_x -= 8
	
	current_direction = next_direction
	
	var new_part := add_part(current_direction, next_x, next_y)
	update_display_and_neighbors(new_part)
	
	if grow <= 0:
		var old_tail := get_tail()
		remove_child(old_tail)
		snake_parts.remove(snake_parts.find(old_tail))
		update_display_and_neighbors(get_tail())
	else:
		grow = grow - 1

func get_head() -> SnakePart:
	return snake_parts[0]

func get_tail() -> SnakePart:
	return snake_parts[-1]

func get_previous(part: SnakePart) -> SnakePart:
	var i := snake_parts.find(part)
	
	#print('get_previous '+ str(i))
	#print('size()       '+ str(snake_parts.size()))
	
	if i + 1 >= snake_parts.size():
		return null
	else:
		return snake_parts[i + 1]

func get_next(part: SnakePart) -> SnakePart:
	var i := snake_parts.find(part)
	
	if i - 1 < 0:
		return null
	else:
		return snake_parts[i - 1]

# Créer et ajouter un nouveau morceau de serpent
# son apparence doit ensuite être mise à jour
func add_part(direction: int, x: int, y: int) -> SnakePart:
	
	# Instanciation
	var snake_part: SnakePart = SnakePart.instance()
	snake_part.index = counter
	counter = counter + 1
	
	# Ajout de l'instance au tableau des morceaux du serpent
	snake_parts.push_front(snake_part)
	
	# Direction du morceau de serpent
	snake_part.direction = direction
	
	# Ajout à la scène (appelle _ready)
	add_child(snake_part)
	
	# Position du morceau
	snake_part.position.x = x
	snake_part.position.y = y

	return snake_part

# Mets à jour l'apparence d'un morceau du serpent
# en fonction de ses éventuels voisins
# et met aussi à jour l'apparence de ces derniers
func update_display_and_neighbors(part: SnakePart) -> void:
	
	# Eventuel morceau précédent
	var previous_part := get_previous(part)
	
	if previous_part != null:
		part.previous_direction = previous_part.direction
		previous_part.next_direction = part.direction
		previous_part.update_display()
	else:
		part.previous_direction = -1
	
	# Eventuel morceau suivant
	var next_part := get_next(part)
	
	if next_part != null:
		part.next_direction = next_part.direction
		next_part.previous_direction = part.direction
		next_part.update_display()
	else:
		part.next_direction = -1
	
	# Mettre à jour l'apparence du morceau de serpent
	part.update_display()
	
