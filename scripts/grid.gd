class_name Grid


const tile_size := 8

func get_pixel_x(x: int):
	return x * tile_size

func get_pixel_y(y: int):
	return y * tile_size

